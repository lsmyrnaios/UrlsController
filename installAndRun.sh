# This script installs and runs the project.

# For error-handling, we cannot use the "set -e" since: it has problems https://mywiki.wooledge.org/BashFAQ/105
# So we have our own function, for use when a single command fails.
handle_error () {
  echo -e "\n\n$1\n\n"; exit $2
}

# Change the working directory to the script's directory, when running from another location.
cd "${0%/*}" || handle_error "Could not change-dir to this script's dir!" 1

justRun=0
shouldRunInDocker=0

if [[ $# -eq 1 ]]; then
	justRun=$1
elif [[ $# -eq 2 ]]; then
  justRun=$1
  shouldRunInDocker=$2
elif [[ $# -gt 2 ]]; then
	echo -e "Wrong number of arguments given: ${#}\nPlease execute it like: installAndRun.sh <justRun: 0 | 1> <shouldRunInDocker: 0 | 1>";	exit 2
fi

if [[ justRun -eq 1 && shouldRunInDocker -eq 1 ]]; then
  echo -e "Cannot run in docker without re-building the project (just to be safe). Setting \"justRun\" to < 0 >"
  justRun=0
fi

gradleVersion="8.12.1"

if [[ justRun -eq 0 ]]; then

  if [[ ! -d /opt/gradle/gradle-${gradleVersion} ]]; then
    wget https://services.gradle.org/distributions/gradle-${gradleVersion}-bin.zip
    echo -e "\nAsking for sudo, in order to install 'gradle'..\n"
    sudo mkdir /opt/gradle
    sudo apt install -y unzip && sudo unzip -d /opt/gradle gradle-${gradleVersion}-bin.zip
    sudo rm -rf gradle-${gradleVersion}-bin.zip
    #ls /opt/gradle/gradle-${gradleVersion}  # For debugging installation
  fi

  export PATH=/opt/gradle/gradle-${gradleVersion}/bin:$PATH

  gradle wrapper --gradle-version=${gradleVersion} --distribution-type=bin

  #gradle tasks  # For debugging installation
  #gradle -v # For debugging installation

  gradle clean build # --refresh-dependencies # --info

  if [[ shouldRunInDocker -eq 1 ]]; then

    runPrometheusAndGrafanaContainers=0 # This is set here, not through cmd-args.

    echo -e "\nBuilding the docker image and running the containers..\n"
    sudo docker --version || handle_error "Docker was not found!" 3
    (sudo mkdir -p "$HOME"/tmp/config && sudo cp ./src/main/resources/application.yml "$HOME"/tmp/config) || true  # This also replaces an existing "application.yml".
    sudo mkdir -p "$HOME"/logs || true

    # Run in "detached mode" -d (in the background).
    (sudo docker compose up --build -d && echo -e "\nThe Urls_Controller docker-container started running.\n") || handle_error "Could not build and/or run the 'urls_controller' docker container!" 4

    if [[ runPrometheusAndGrafanaContainers -eq 1 ]]; then
        (sudo docker compose -f prometheus/docker-compose-prometheus.yml up --build -d && echo -e "\nThe Prometheus and Grafana docker-containers started running.\n") || handle_error "Could not build and/or run the 'prometheus' and/or 'grafana' docker containers!" 5
    fi

    echo -e "Waiting 20 seconds before getting the status..\n"
    sleep 20
    # 20 seconds are enough to check if there is an immediate fatal error with one of the docker images. This will cover the problematic configuration case for Prometheus and Grafana containers.
    sudo docker ps -a || handle_error "Could not get the status of docker-containers!" 6  # Using -a to get the status of failed containers as well.
    echo -e "\n\nGetting the logs of docker-container \"urls_controller\":\n"
    sudo docker logs -f urls_controller || handle_error "Could not get the logs of docker-container \"urls_controller\"!" 7  # Using "regex anchors" to avoid false-positives. Works even if the container is not running, thus showing the error-log.
    # Use just the container-name and the "-f" parameter to indicate that we want to follow on logs updates, until we specify to unfollow them (with ctrl+c).
    # This way we do not need to run the "docker logs" again and again, not checking the the container-id each time.
  fi
else
  export PATH=/opt/gradle/gradle-${gradleVersion}/bin:$PATH # Make sure the gradle is still accessible (it usually isn't without the "export").
fi

if [[ shouldRunInDocker -ne 1 ]]; then
  gradle bootRun
fi
