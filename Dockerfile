FROM eclipse-temurin:17-jre-alpine

COPY build/libs/UrlsController-*.jar urls_controller.jar

EXPOSE 1880

ENTRYPOINT ["java", "-Xms1024m", "-Xmx6144m", "-jar", "/urls_controller.jar", "--spring.config.location=file:///mnt/config/application.yml"]
