# Visualize Prometheus metrics

### Requirements
- Run the docker-compose of the Controller.
- Run the docker-compose of prometheus.


### Check the metrics
- To check the raw metrics check the output of this url, in your browser: http://\<IP\>:1880/api/actuator/prometheus
- To check the metrics though GUI, with some graphs, check: http://\<IP\>:9090


### Visualize metrics in Grafana
- Access grafana in:  http://\<IP\>:3000/
- Give the default username and password: "admin" (for both).
- Specify the new password.
- Then, add a "Prometheus" datasource.
- Specify the prometheus url: http://\<IP\>:9090
- Save the datasource.
- Go to "dashboards", click "import", add the number "12900" in the input and click "load" and "next".
- Then select the "prometheus" datasource and click "import".
- Enjoy the graphs.

