# This script shuts down (ONLY!) the Controller, by stopping and killing the related containers.
# It is used during testing.
# It does not shuts down the whole service! The workers will keep running and their work will be lost.

# For error-handling, we cannot use the "set -e" since: it has problems https://mywiki.wooledge.org/BashFAQ/105
# So we have our own function, for use when a single command fails.
handle_error () {
  echo -e "\n\n$1\n\n"; exit $2
}

# Change the working directory to the script's directory, when running from another location.
cd "${0%/*}" || handle_error "Could not change-dir to this script's dir!" 1

forceControllerShutdown=0

if [[ $# -eq 1 ]]; then
	forceControllerShutdown=$1
elif [[ $# -gt 1 ]]; then
	echo -e "Wrong number of arguments given: ${#}\nPlease execute it like: shutdownService.sh <forceControllerShutdown: 0 | 1>";	exit 2
fi
# We may have no arguments, if we do not want to force the Controller to shutdown.

# Shutdown Prometheus, if it's running.
sudo docker compose -f ./prometheus/docker-compose-prometheus.yml down

if [[ forceControllerShutdown -eq 1 ]]; then
  echo "Shutting down the Controller.."
  sudo docker compose -f docker-compose.yml down
else
  echo "Shutting down the Service.."
  sudo apt install -y curl
  curl -X POST -i 'http://localhost:1880/api/shutdownService'

  # Follow the logs until shutdown.
  sudo docker logs -f urls_controller || handle_error "Could not get the logs of docker-container \"urls_controller\"!" 3  # Using "regex anchors" to avoid false-positives. Works even if the container is not running, thus showing the error-log.
fi

# In case we need to hard-remove the containers, use the following commands:
#sudo docker stop $(sudo docker ps -aqf "name=^(?:urlscontroller-urls_controller|prometheus-(?:prometheus|grafana))-1$") || true # There may be no active containers
#sudo docker rm $(sudo docker ps -aqf "name=^(?:urlscontroller-urls_controller|prometheus-(?:prometheus|grafana))-1$") || true # All containers may be already removed.
