package eu.openaire.urls_controller.services;

import eu.openaire.urls_controller.components.BulkImport;
import eu.openaire.urls_controller.models.BulkImportReport;

import java.io.File;
import java.util.List;

public interface BulkImportService {


    Boolean bulkImportFullTextsFromDirectory(BulkImportReport bulkImportReport, String relativeBulkImportDir, String bulkImportDirName, File bulkImportDir, String provenance, BulkImport.BulkImportSource bulkImportSource, boolean shouldDeleteFilesOnFinish);

    List<String> getFileLocationsInsideDir(String directory, String idMappingsFilePath);

    String getMD5Hash(String string);

}
