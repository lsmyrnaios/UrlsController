package eu.openaire.urls_controller.services;

import eu.openaire.urls_controller.models.UrlReport;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface UrlsService {

    ResponseEntity<?> getAssignments(String workerId, int assignmentsLimit);

    Boolean addWorkerReport(String curWorkerId, long curReportAssignments, List<UrlReport> urlReports, int sizeOfUrlReports);

}
