package eu.openaire.urls_controller.services;

import org.springframework.http.ResponseEntity;

public interface StatsService {

    ResponseEntity<?> getNumberOfPayloads(String getPayloadsNumberQuery, String extraMsg, int retryCount);

    ResponseEntity<?> getNumberOfRecordsInspectedByServiceThroughCrawling(int retryCount);

}
