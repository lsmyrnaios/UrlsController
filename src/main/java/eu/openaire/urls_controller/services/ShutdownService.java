package eu.openaire.urls_controller.services;

import org.springframework.http.ResponseEntity;

public interface ShutdownService {

    ResponseEntity<?> passSecurityChecks(String remoteAddr, String initMsg);

    void postShutdownOrCancelRequestsToAllWorkers(boolean shouldCancel);

    boolean postShutdownOrCancelRequestToWorker(String workerId, String workerIp, boolean shouldCancel);

	boolean getShouldShutdownService();

	void setShouldShutdownService(boolean shouldShutdownService);
}
