package eu.openaire.urls_controller.util;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class JsonUtils {

	private static final Logger logger = LoggerFactory.getLogger(JsonUtils.class);

	private static final Gson gson = new Gson();    // This is "transient" by default. It won't be included in any json object.


	public ConcurrentHashMap<String, String> loadIdMappings(String filePath, int mappingsSize, String additional_message)
	{
		logger.debug("Going to load the idMappings from '" + filePath + "'." + additional_message);
		ConcurrentHashMap<String, String> idMappings = new ConcurrentHashMap<>(mappingsSize);
		try ( BufferedReader br = new BufferedReader(new FileReader(filePath), FileUtils.halfMb) ) {
			JsonArray idMappingsList = gson.fromJson(br, JsonArray.class);

			//logger.debug("IdMappingsList:\n" + idMappingsList);	// DEBUG!

			for ( JsonElement idMapping : idMappingsList ) {
				JsonObject jsonObject = idMapping.getAsJsonObject();
				if ( null != idMappings.put(jsonObject.get("file").getAsString(), jsonObject.get("id").getAsString()) )
					logger.warn("There was a duplicate file '" + jsonObject.get("file") + "' (probably in a different sub-directory)!" + additional_message);
			}

			/*Function<JsonObject, String> keyMapper = key -> key.get("file").getAsString();
			Function<JsonObject, String> valueMapper = value -> value.get("id").getAsString();
			idMappings = (ConcurrentHashMap<String, String>) idMappingsList.asList().stream()
					.flatMap(jsonElement -> Stream.of(jsonElement.getAsJsonObject()))
					.collect(Collectors.toConcurrentMap(keyMapper, valueMapper,
							(id1, id2) -> {logger.warn("There was a duplicate file '" + keyMapper.apply(id1) + "' (probably in a different sub-directory)!" + additional_message);; return id1;} ));	// Keep the first-assigned id, for this duplicate file.
			// TODO - How to get the KEY Inside the keyMapper..?
*/
		} catch (FileNotFoundException fnfe) {
			logger.error("Could not find the id-file-idMappings! " + fnfe.getMessage() + additional_message);
		} catch (Exception e) {
			logger.error("Could not load the id-file-idMappings!" + additional_message, e);
			try ( BufferedReader br = new BufferedReader(new FileReader(filePath), FileUtils.halfMb) ) {
				logger.warn(br.readLine() + br.readLine() + br.readLine());
			} catch (Exception ex) {
				logger.error("", ex);
			}
		}

		//if ( idMappings != null )	// Uncomment, in case the "stream"-version is used.
			//logger.debug("IdMappings:\n" + idMappings);	// DEBUG!

		return idMappings;	// It may be null.
	}

}
