package eu.openaire.urls_controller.util;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.SetMultimap;
import eu.openaire.urls_controller.components.ScheduledTasks;
import eu.openaire.urls_controller.configuration.DatabaseConnector;
import eu.openaire.urls_controller.models.Error;
import eu.openaire.urls_controller.models.Payload;
import eu.openaire.urls_controller.models.UrlReport;
import org.apache.commons.io.FileDeleteStrategy;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.io.*;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class FileUtils {

    private static final Logger logger = LoggerFactory.getLogger(FileUtils.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private S3ObjectStore s3ObjectStore;


    public enum UploadFullTextsResponse {successful, successful_without_fulltexts, unsuccessful}


    public static final String workingDir = System.getProperty("user.dir") + File.separator;

    private final boolean isTestEnvironment;


    public FileUtils (@Value("${services.pdfaggregation.controller.isTestEnvironment}") boolean isTestEnvironment) {

        this.isTestEnvironment = isTestEnvironment;
    }


    // The following regex might be useful in a future scenario. It extracts the "plain-filename" and "file-ID" and the "file-extension".
    // Possible full-filenames are: "path1/path2/ID.pdf", "ID2.pdf", "path1/path2/ID(12).pdf", "ID2(25).pdf"
    public static final Pattern FILEPATH_ID_EXTENSION = Pattern.compile("(.*/)?((([^/()]+)[^./]*)(\\.[\\w]{2,10}))$");

    public static final ExecutorService hashMatchingExecutor = Executors.newFixedThreadPool(6);
    // TODO - Unify this ExecutorService with the hash-matching executorService. Since one will ALWAYS be called after the other. So why having two ExecServices to handle?


    public HashMultimap<String, Payload> getHashesWithPayloads(List<UrlReport> urlReports, int sizeOfUrlReports)
    {
        HashMultimap<String, Payload> hashesWithPayloads = HashMultimap.create((sizeOfUrlReports / 5), 3);   // Holds multiple payloads for the same fileHash.
        // The "Hash" part of the multimap helps with avoiding duplicate fileHashes.

        for ( UrlReport urlReport : urlReports )
        {
            Payload payload = urlReport.getPayload();
            if ( payload == null )
                continue;

            String fileLocation = payload.getLocation();
            if ( fileLocation == null )
                continue;   // The full-text was not retrieved for this UrlReport.

            // Query the payload-table FOR EACH RECORD to get the fileLocation of A PREVIOUS RECORD WITH THE SAME FILE-HASH.
            // If no result is returned, then this record is not previously found, so go ahead and add it in the list of files to request from the worker.
            // If a file-location IS returned (for this hash), then this file is already uploaded to the S3. Update the record to point to that file-location and do not request that file from the Worker.
            String fileHash = payload.getHash();
            if ( fileHash != null )
            {
                hashesWithPayloads.put(fileHash, payload);  // Hold multiple payloads per fileHash.
                // There are 2 cases, which contribute to that:
                // 1) Different publication-IDs end up giving the same full-text-url, resulting in the same file. Those duplicates are not saved, but instead, the location, hash and size of the file is copied to the other payload.
                // 2) Different publication-IDs end up giving different full-text-urls which point to the same file. Although very rare, in this case, the file is downloaded again by the Worker and has a different name.

                // In either case, the duplicate file will not be transferred to the Controller, but in the 2nd one it takes up extra space, at least for some time.
                // TODO - Implement a fileHash-check algorithm in the Worker's side ("PublicationsRetriever"), to avoid keeping those files in storage.

            } else  // This should never happen..
                logger.error("Payload: " + payload + " has a null fileHash!");
        }// end-for

        return hashesWithPayloads;
    }


    public HashMap<String, String> getHashLocationMap(Set<String> fileHashes, int fileHashesSetSize, long batchCounter, String groupType)
    {
        // Prepare the "fileHashListString" to be used inside the "getHashLocationsQuery". Create the following string-pattern:
        // ("HASH_1", "HASH_2", ...)
        int stringBuilderCapacity = ((fileHashesSetSize * 32) + (fileHashesSetSize -1) +2);

        String getHashLocationsQuery = "select distinct `hash`, `location` from " + DatabaseConnector.databaseName + ".payload where `hash` in "
                + getQueryListString(fileHashes, fileHashesSetSize, stringBuilderCapacity);

        HashMap<String, String> hashLocationMap = new HashMap<>(fileHashesSetSize/2);   // No multimap is needed since only one location is returned for each fileHash.

        DatabaseConnector.databaseLock.lock();    // The execution uses the database.
        try {
            jdbcTemplate.query(getHashLocationsQuery, rs -> {
                try {   // For each of the 4 columns returned, do the following. The column-indexing starts from 1.
                    hashLocationMap.put(rs.getString(1), rs.getString(2));
                } catch (SQLException sqle) {
                    logger.error("No value was able to be retrieved from one of the columns of row_" + rs.getRow(), sqle);
                }
            });
        } catch (EmptyResultDataAccessException erdae) {
            logger.warn("No previously-found hash-locations where found for " + groupType + "_" + batchCounter);
        } catch (Exception e) {
            logger.error("Unexpected error when checking for already-found file-hashes, for " + groupType + "_" + batchCounter, e);
            // We will continue with storing the files, we do not want to lose them.
        } finally {
            DatabaseConnector.databaseLock.unlock();
        }

        return hashLocationMap;
    }


    void uploadFullTexts(String[] fileNames, String targetDirectory, SetMultimap<String, Payload> allFileNamesWithPayloads, int batchCounter)
    {
        // Iterate over the files and upload them to S3.

        // TODO - Make the uploads run in parallel, using at most 4 threads.
        // There might also be uploads from other assignment-batches (from other workers) running at the same time.
        // But still, there are currently at most 3 (?) upload operations, ast the same time, each using 1 thread.
        // It's most common to have just 1 or 2 of them. So each can easily use 2 or 4 threads to upload its own files.

        //int numUploadedFiles = 0;
        for ( String fileName : fileNames )
        {
            // Check if any of the ".tar" files (".tar" and ".tar.zstd") are still in the directory (in case their deletion failed) and exclude them from being uploaded.
            if ( fileName.contains(".tar") )
                continue;

            // Check if this stored file is related to one or more Payloads from the Set. Defend against malicious file injection. It does not add more overhead, since we already need the "fileRelatedPayloads".
            Set<Payload> fileRelatedPayloads = allFileNamesWithPayloads.get(fileName);
            if ( fileRelatedPayloads.isEmpty() ) {  // In case the "fileName" is not inside the "allFileNamesWithPayloads" HashMultimap.
                logger.error("The stored file \"" + fileName + "\" is not related to any Payload returned from the Worker!");
                continue;
            }

            // Let's try to upload the file to S3 and update the payloads, either in successful file-uploads (right-away) or not (in the end).

            // Prepare the filename as: "datasourceid/publicationid::hash.pdf"
            // All related payloads point to this exact same file, BUT, may be related with different urlIDs, which in turn be related with different datasourceIDs.
            // This file could have been found from different urlIds and thus be related to multiple datasourceIds.
            // BUT, since the filename contains a specific urlID, the datasourceId should be the one related to that specific urlID.
            // So, we extract this urlID, search the payload inside the "fileRelatedPayloads" and get the related datasourceID (instead of taking the first or a random datasourceID).

            Matcher matcher = FILEPATH_ID_EXTENSION.matcher(fileName);
            if ( !matcher.matches() ) {
                logger.error("Failed to match the \"" + fileName + "\" with the regex: " + FILEPATH_ID_EXTENSION);
                continue;
            }
            // The "matcher.group(3)" returns the "filenameWithoutExtension", which is currently not used.
            // Use the "fileNameID" and not the "filenameWithoutExtension", as we want to avoid keeping the possible "parenthesis" with the increasing number (about the duplication of ID-fileName).
            String fileNameID = matcher.group(4);   // The "fileNameID" is the OpenAIRE_ID for this file.
            if ( (fileNameID == null) || fileNameID.isEmpty() ) {
                logger.error("Failed to extract the \"fileNameID\" from \"" + fileName + "\".");
                continue;
            }
            String dotFileExtension = matcher.group(5);
            if ( (dotFileExtension == null) || dotFileExtension.isEmpty() ) {
                logger.error("Failed to extract the \"dotFileExtension\" from \"" + fileName + "\".");
                continue;
            }

            // This file is related with some payloads, in a sense that these payloads have urls which lead to the same full-text url.
            // These payloads might have different IDs and sourceUrls. But, in the end, the different sourceUrls give the same full-text.
            // Below, we make sure we pick the "datasource" from the payload, which has the same id as the full-text's name.
            // If there are multiple payloads with the same id, which point to the same file, then we can take whatever datasource we want from those payloads.
            // It is possible that payloads with same IDs, but different sourceUrls pointing to the same full-text, can be related with different datasources
            // (especially for IDs of type: "doiboost_____::XXXXXXXXXXXXXXXXXXXXX").
            // It does not really matter, since the first-ever payload to give this full-text could very well be another one,
            // since the crawling happens in multiple threads which compete with each other for CPU time.

            String datasourceId = null;
            String hash = null;
            String mimeType = null;
            boolean isFound = false;
            for ( Payload payload : fileRelatedPayloads ) {
                if ( fileNameID.equals(payload.getId()) ) {
                    datasourceId = payload.getDatasourceId();
                    hash = payload.getHash();
                    mimeType = payload.getMime_type();
                    isFound = true;
                    break;
                }
            }

            if ( !isFound ) {  // This should never normally happen. If it does, then a very bad change will have taken place.
                logger.error("The \"fileNameID\" (" + fileNameID + ") was not found inside the \"fileRelatedPayloads\" for fileName: " + fileName);
                continue;
            }

            try {
                String s3Url = constructS3FilenameAndUploadToS3(targetDirectory, fileName, fileNameID, dotFileExtension, datasourceId, hash, mimeType);
                if ( s3Url != null ) {
                    setFullTextForMultiplePayloads(fileRelatedPayloads, s3Url);
                    //numUploadedFiles ++;
                }
            } catch (Exception e) {
                logger.error("Avoid uploading the rest of the files of batch_" + batchCounter + " | " + e.getMessage());
                break;
            }
            // Else, the record will have its file-data set to "null", in the end of the caller method (as it will not have an s3Url as its location).
        }//end filenames-for-loop

        //logger.debug("Finished uploading " + numUploadedFiles + " full-texts (out of " + fileNames.length + " distinct files) from assignments_" + assignmentsBatchCounter + ", batch_" + batchCounter + " on S3-ObjectStore.");
    }


    public String constructS3FilenameAndUploadToS3(String targetDirectory, String fileName, String openAireId,
                                                   String dotFileExtension, String datasourceId, String hash, String mimeType) throws ConnectException, UnknownHostException
    {
        String filenameForS3 = constructS3FileName(fileName, openAireId, dotFileExtension, datasourceId, hash);   // This name is for the uploaded file, in the S3 Object Store.
        if ( filenameForS3 == null )    // The error is logged inside.
            return null;

        String fileFullPath = targetDirectory + fileName;   // The fullPath to the local file (which has the previous name).
        String s3Url = null;
        try {
            s3Url = s3ObjectStore.uploadToS3(filenameForS3, fileFullPath, mimeType, 0);
        } catch (ConnectException ce) {
            logger.error("Could not connect with the S3 Object Store! " + ce.getMessage());
            throw ce;
        } catch (UnknownHostException uhe) {
            logger.error("The S3 Object Store could not be found! " + uhe.getMessage());
            throw uhe;
        } catch (Exception e) {
            logger.error("Could not upload the local-file \"" + fileFullPath + "\" to the S3 ObjectStore, with S3-filename: \"" + filenameForS3 + "\"!", e);
            return null;
        }
        return s3Url;
    }


    public String constructS3FileName(String fileName, String openAireID, String dotFileExtension, String datasourceId, String hash)
    {
        if ( datasourceId == null ) {
            logger.error("The retrieved \"datasourceId\" was \"null\" for file: " + fileName);
            return null;
        }
        if ( hash == null ) {
            logger.error("The retrieved \"hash\" was \"null\" for file: " + fileName);
            return null;
        }

        // Now we append the file-hash, so it is guaranteed that the filename will be unique.
        return (datasourceId + "/" + openAireID + "::" + hash + dotFileExtension);  // This is the fileName to be used in the objectStore, not of the local file!
    }


    public String getMessageFromResponseBody(HttpURLConnection conn, boolean isError)
    {
        final StringBuilder msgStrB = new StringBuilder(500);
        try ( BufferedReader br = new BufferedReader(new InputStreamReader((isError ? conn.getErrorStream() : conn.getInputStream()), StandardCharsets.UTF_8)) ) {
            String inputLine;
            while ( ((inputLine = br.readLine()) != null) && !inputLine.isEmpty() ) {
                msgStrB.append(inputLine);
            }
            return (msgStrB.length() != 0) ? msgStrB.toString() : null;	// Make sure we return a "null" on empty string, to better handle the case in the caller-function.
        } catch ( IOException ioe ) {
            logger.error("IOException when retrieving the response-body: " + ioe.getMessage());
            return null;
        } catch ( Exception e ) {   // This includes the case, where the "conn.getErrorStream()" returns < null >.
            logger.error("Could not extract the response-body!", e);
            return null;
        }
    }


    public static final int twentyFiveKb = 25_600;   // 25 Kb
    public static final int halfMb = 524_288;   // 0.5 Mb = 512 Kb = 524_288 bytes
    public static final int tenMb = (10 * 1_048_576);

    public boolean saveArchive(InputStream inputStream, File zstdFile)
    {
        long numBytesOfZstdFile = 0;
        try ( BufferedInputStream inStream = new BufferedInputStream(inputStream, tenMb);
              BufferedOutputStream outStream = new BufferedOutputStream(Files.newOutputStream(zstdFile.toPath()), tenMb) )
        {
            int readBytes;
            while ( (readBytes = inStream.read()) != -1 ) {
                outStream.write(readBytes);
                numBytesOfZstdFile ++;
            }
            ScheduledTasks.totalFulltextWorkerReportSize.addAndGet(numBytesOfZstdFile);  // Add this zstd size in order to get the "average", for workerReports, later.
            return true;
        } catch (Exception e) {
            logger.error("Could not save the zstd file \"" + zstdFile.getName() + "\": " + e.getMessage(), e);
            return false;
        }
    }


    /**
     * This method searches the backlog of publications for the ones that have the same "original_url" or their "original_url" is equal to the "actual_url" or an existing payload.
     * Then, it generates a new "UrlReport" object, which has as a payload a previously generated one, which has different "id", "original_url".
     * Then, the program automatically generates "attempt" and "payload" records for these additional UrlReport-records.
     * It must be executed inside the same "database-locked" block of code, along with the inserts of the attempt and payload records.
     * */
    public boolean addUrlReportsByMatchingRecordsFromBacklog(List<UrlReport> urlReports, List<Payload> initialPayloads, int numInitialPayloads, long assignmentsBatchCounter)
    {
        logger.debug("numInitialPayloads: " + numInitialPayloads + " | assignmentsBatchCounter: " + assignmentsBatchCounter);

        // Create a HashMultimap, containing the "original_url" or "actual_url" as the key and the related "payload" objects as its values.
        final HashMultimap<String, Payload> urlToPayloadsMultimap = HashMultimap.create((numInitialPayloads / 3), 3);    // Holds multiple values for any key, if a fileName(key) has many IDs(values) associated with it.
        for ( Payload payload : initialPayloads ) {
            String original_url = payload.getOriginal_url();
            String actual_url = payload.getActual_url();

            // Link this payload with both the original and actual urls (in case they are different).
            urlToPayloadsMultimap.put(original_url, payload);
            if ( ! actual_url.equals(original_url) )
                urlToPayloadsMultimap.put(actual_url, payload);
        }
        // A url may be related to different payloads, in the urlReports. For example, in one payload the url was the original_url
        // but in another payload the url was only the actual-url (the original was different)

        // Gather the original and actual urls of the current payloads and add them in a list usable by the query.
        // Some of the original_urls are the same with their actual_urls or the actual_urls may be the same with the original_urls of other payloads!
        // But, by using a (Hash)Set or urls, distinct urls are guaranteed.
        Set<String> urlsToRetrieveRelatedIDs = urlToPayloadsMultimap.keySet();

        if ( logger.isTraceEnabled() )
            logger.trace(urlsToRetrieveRelatedIDs.toString());

        // Prepare the "urlsToRetrieveRelatedIDs" to be used inside the "getDataForPayloadPrefillQuery". Create the following string-pattern: ("URL_1", "URL_2", ...)
        int urlsToRetrieveRelatedIDsSize = urlsToRetrieveRelatedIDs.size();
        int stringBuilderCapacity = (urlsToRetrieveRelatedIDsSize * 100);

        // Get the id and url of any records which have not been processed yet and have their url
        String getDataForPayloadPrefillQuery = "select distinct pu.id, pu.url\n" +
                "from " + DatabaseConnector.databaseName + ".publication_urls pu\n" +
                // Exclude the "already-processed" pairs.
                "left anti join " + DatabaseConnector.databaseName + ".attempt a on a.id=pu.id and a.original_url=pu.url\n" +
                "left anti join " + DatabaseConnector.databaseName + ".payload p on p.id=pu.id and p.original_url=pu.url\n" +
                "left anti join " + DatabaseConnector.databaseName + ".assignment asgn on asgn.id=pu.id and asgn.original_url=pu.url\n" +
                // Limit the urls to the ones matching to the payload-urls found for the current assignments.
                "where pu.url in " + getQueryListString(urlsToRetrieveRelatedIDs, urlsToRetrieveRelatedIDsSize, stringBuilderCapacity);

        //logger.trace("getDataForPayloadPrefillQuery:\n" + getDataForPayloadPrefillQuery);

        final List<Payload> prefilledPayloads = new ArrayList<>(1000);
        try {
            jdbcTemplate.query(getDataForPayloadPrefillQuery, rs -> {
                String id;
                String original_url;
                try {   // For each of the 2 columns returned, do the following. The column-indexing starts from 1.
                    id = rs.getString(1);
                    original_url = rs.getString(2);
                } catch (SQLException sqle) {
                    logger.error("No value was able to be retrieved from one of the columns of row_" + rs.getRow(), sqle);
                    return; // Move to the next payload.
                }
                Set<Payload> foundPayloads = urlToPayloadsMultimap.get(original_url);   // It doesn't return null, on error, but an empty set.
                if ( foundPayloads.size() == 0 ) {
                    logger.error("No payloads associated with \"original_url\" = \"" + original_url + "\"!");
                    return;
                }
                // Select a random "foundPayload" to use its data to fill the "prefilledPayload" (in a "Set" the first element is pseudo-random).
                // Since the "foundPayloads" is not-empty at this point, the, we do not have to check if the first element is present.
                Payload prefilledPayload = foundPayloads.stream().findFirst().get().clone();    // We take a clone of the original payload and change just the id and the original_url.
                prefilledPayload.setId(id); // The prefilled payload will have the non-yet-processed id and url from the Graph.
                prefilledPayload.setOriginal_url(original_url);
                prefilledPayloads.add(prefilledPayload);
            });
        } catch (EmptyResultDataAccessException erdae) {
            logger.error("No results retrieved from the \"getDataForPayloadPrefillQuery\", when trying to prefill payloads, from assignment_" + assignmentsBatchCounter + ".");
            return false;
        } catch (Exception e) {
            DatabaseConnector.handleQueryException("getDataForPayloadPrefillQuery", getDataForPayloadPrefillQuery, e);
            return false;
        }

        int numPrefilledPayloads = prefilledPayloads.size();
        if ( numPrefilledPayloads == 0 ) {
            logger.error("Some results were retrieved from the \"getDataForPayloadPrefillQuery\", but no data could be extracted from them, when trying to prefill payloads, from assignment_" + assignmentsBatchCounter + ".");
            return false;
        }

        logger.debug("numPrefilledPayloads: " + numPrefilledPayloads + " | assignmentsBatchCounter: " + assignmentsBatchCounter);

        // Add the prefilled to the UrlReports.
        final Error noError = new Error(null, null);
        for ( Payload prefilledPayload : prefilledPayloads )
        {
            urlReports.add(new UrlReport(UrlReport.StatusType.accessible, prefilledPayload, noError));
        }

        logger.debug("Final number of UrlReports is " + urlReports.size() + " | assignmentsBatchCounter: " + assignmentsBatchCounter);

        // In order to avoid assigning these "prefill" records to workers, before they are inserted in the attempt and payload tables..
        // We have to make sure this method is called inside a "DB-locked" code block and the "DB-unlock" happens only after all records are loaded into the DB-tables.
        return true;
    }


    /**
     * Set the fileLocation for all those Payloads related to the File.
     * @param filePayloads
     * @param s3Url
     */
    public void setFullTextForMultiplePayloads(@NotNull Set<Payload> filePayloads, String s3Url) {
        for ( Payload payload : filePayloads )
            if ( payload != null )
                payload.setLocation(s3Url); // Update the file-location to the new S3-url. All the other file-data is already set from the Worker.
    }


    public boolean deleteDirectory(File directory)
    {
        try {
            org.apache.commons.io.FileUtils.deleteDirectory(directory);
            return true;    // Will return "true" also in case this directory does not exist. So, no Exception will be thrown for that case.
        } catch (IOException e) {
            logger.error("The following directory could not be deleted: " + directory.getName(), e);
            return false;
        } catch (IllegalArgumentException iae) {
            logger.error("This batch-dir does not exist: " + directory.getName());
            return false;
        }
    }


    public boolean deleteFile(String fileFullPathString)
    {
        try {
            FileDeleteStrategy.FORCE.delete(new File(fileFullPathString));
        } catch (IOException e) {
            logger.error("Error when deleting the file: " + fileFullPathString);
            return false;
        }
        return true;
    }


    public static final Lock fileAccessLock = new ReentrantLock(true);

    public String writeToFile(String fileFullPath, String stringToWrite, boolean shouldLockThreads)
    {
        if ( stringToWrite == null )
            return "The string to write to file '" + fileFullPath + "' is null!";

        if ( shouldLockThreads )    // In case multiple threads write to the same file. for ex. during the bulk-import procedure.
            fileAccessLock.lock();

        // TODO - Make this method to be synchronized for the specific file, not in general.
        // TODO - NOW: Multiple bulkImport procedures (with diff DIRs), are blocked while writing to DIFFERENT files..

        try ( BufferedWriter bufferedWriter = new BufferedWriter(Files.newBufferedWriter(Paths.get(fileFullPath)), halfMb) )
        {
            bufferedWriter.write(stringToWrite); // This will overwrite the file. If the new string is smaller, then it does not matter.
        } catch (Exception e) {
            String errorMsg = "Failed to create or acquire the file \"" + fileFullPath + "\"!";
            logger.error(errorMsg, e);
            return errorMsg;
        } finally {
            if ( shouldLockThreads )
                fileAccessLock.unlock();
        }
        return null;
    }


    public static String getQueryListString(Set<String> set, int fileHashesSetSize, int stringBuilderCapacity) {
        StringBuilder sb = new StringBuilder(stringBuilderCapacity);
        sb.append("(");
        int currentIndex = 0;
        for (String hash : set) {
            sb.append("\"").append(hash).append("\"");
            if (++currentIndex < fileHashesSetSize) {
                sb.append(", ");
            }
        }
        sb.append(")");
        return sb.toString();
    }

}
