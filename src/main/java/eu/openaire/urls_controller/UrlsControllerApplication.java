package eu.openaire.urls_controller;

import com.zaxxer.hikari.HikariDataSource;
import eu.openaire.urls_controller.controllers.BulkImportController;
import eu.openaire.urls_controller.controllers.UrlsController;
import eu.openaire.urls_controller.services.UrlsServiceImpl;
import eu.openaire.urls_controller.util.FileUtils;
import eu.openaire.urls_controller.util.UriBuilder;
import io.micrometer.core.aop.TimedAspect;
import io.micrometer.core.instrument.MeterRegistry;
import jakarta.annotation.PreDestroy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.context.ServletWebServerApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

@SpringBootApplication
@EnableScheduling
public class UrlsControllerApplication {

    private static final Logger logger = LoggerFactory.getLogger(UrlsControllerApplication.class);

    @Autowired
    HikariDataSource hikariDataSource;

    private static ConfigurableApplicationContext context;


    public static void main(String[] args) {
        context = SpringApplication.run(UrlsControllerApplication.class, args);
    }


    public void gentleAppShutdown()
    {
        logger.info("Shutting down the app..");
        shutdownThreads();
        int exitCode = 0;
        try {
            exitCode = SpringApplication.exit(context, () -> 0);    // The "PreDestroy" method will be called. (the "context" will be closed automatically (I checked it))
        } catch (IllegalArgumentException iae) {
            String message = iae.getMessage();
            if ( (message != null) && !message.contains("Context must not be null") ) // This will be the case when the "gentleAppShutdown()" was called too early in the app's lifetime, which is ok.
                logger.error("", iae);
        }
        System.exit(exitCode);
    }


    private boolean haveThreadsShutdown = false;

    @PreDestroy
    public void shutdownThreads()
    {
        // Normally this methods will have already been executed by "gentleAppShutdown()" just before the "PreDestroy" has called this method again.
        // BUT, in case the service was shutdown from the OS, without the use of the "ShutdownService-API, then this will be the 1st time this method is called.
        if ( haveThreadsShutdown )
            return;

        logger.info("Shutting down the threads..");

        shutdownThreadsForExecutorService(UrlsServiceImpl.insertsExecutor);
        shutdownThreadsForExecutorService(FileUtils.hashMatchingExecutor);
        shutdownThreadsForExecutorService(BulkImportController.bulkImportExecutor);
        shutdownThreadsForExecutorService(UrlsController.backgroundExecutor);

        haveThreadsShutdown = true;

        // For some reason the Hikari Datasource cannot close properly by Spring Boot, unless we explicitly call close here.
        hikariDataSource.close();

        logger.info("Exiting..");
    }


    private void shutdownThreadsForExecutorService(ExecutorService executorService) throws RuntimeException
    {
        if ( executorService == null )  // This method was called before the executorService was created.
            return;

        executorService.shutdown();	// Define that no new tasks will be scheduled.
        try {
            if ( ! executorService.awaitTermination(2, TimeUnit.MINUTES) ) {
                logger.warn("The working threads did not finish on time! Stopping them immediately..");
                executorService.shutdownNow();
                // Wait a while for tasks to respond to being cancelled (thus terminated).
                if ( ! executorService.awaitTermination(1, TimeUnit.MINUTES) )
                    logger.warn("The executor " + executorService + " could not be terminated!");
            }
        } catch (SecurityException se) {
            logger.error("Could not shutdown the threads in any way..!", se);
        } catch (InterruptedException ie) {
            try {
                executorService.shutdownNow();
                // Wait a while for tasks to respond to being cancelled (thus terminated).
                if ( ! executorService.awaitTermination(1, TimeUnit.MINUTES) )
                    logger.warn("The executor " + executorService + " could not be terminated!");
            } catch (SecurityException se) {
                logger.error("Could not shutdown the threads in any way..!", se);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }


    @Bean
    public CommandLineRunner setServerBaseUrl(Environment environment, ServletWebServerApplicationContext webServerAppCtxt)
    {
        return args -> new UriBuilder(environment, webServerAppCtxt);
    }


    @Bean
    public TimedAspect timedAspect(MeterRegistry registry) {
        return new TimedAspect(registry);
    }

}