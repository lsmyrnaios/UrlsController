package eu.openaire.urls_controller.payloads.requests;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.gson.Gson;
import eu.openaire.urls_controller.models.UrlReport;

import java.util.List;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "workerId",
        "assignmentId",
        "urlReports"
})
public class WorkerReport {

    private static final Gson gson = new Gson();    // This is "transient" be default. It won't be included in any json object.

    @JsonProperty("workerId")
    private String workerId;

    @JsonProperty("assignmentRequestCounter")
    private Long assignmentRequestCounter;

    @JsonProperty("urlReports")
    private List<UrlReport> urlReports;

    public WorkerReport(String workerId, Long assignmentRequestCounter, List<UrlReport> urlReports) {
        this.workerId = workerId;
        this.assignmentRequestCounter = assignmentRequestCounter;
        this.urlReports = urlReports;
    }

    public String getWorkerId() {
        return workerId;
    }

    public void setWorkerId(String workerId) {
        this.workerId = workerId;
    }

    public Long getAssignmentRequestCounter() {
        return assignmentRequestCounter;
    }

    public void setAssignmentRequestCounter(Long assignmentRequestCounter) {
        this.assignmentRequestCounter = assignmentRequestCounter;
    }

    public List<UrlReport> getUrlReports() {
        return this.urlReports;
    }

    public void setUrlReports(List<UrlReport> urlReports) {
        this.urlReports = urlReports;
    }


    public String getJsonReport() {
        return gson.toJson(this);
    }

    @Override
    public String toString() {
        return "WorkerReport{" +
                "workerId='" + workerId + '\'' +
                ", assignmentRequestCounter=" + assignmentRequestCounter +
                ", urlReports=" + urlReports +
                '}';
    }
}
