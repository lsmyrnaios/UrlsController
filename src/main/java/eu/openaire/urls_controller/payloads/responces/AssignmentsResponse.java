package eu.openaire.urls_controller.payloads.responces;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import eu.openaire.urls_controller.models.Assignment;

import java.util.List;


@JsonInclude(JsonInclude.Include.NON_NULL)
public class AssignmentsResponse {

    @JsonProperty("assignmentsCounter")
    private Long assignmentsCounter;

    @JsonProperty("assignments")
    private List<Assignment> assignments;


    public AssignmentsResponse(Long assignmentCounter, List<Assignment> assignments) {
        this.assignmentsCounter = assignmentCounter;
        this.assignments = assignments;
    }

    public Long getAssignmentsCounter() {
        return assignmentsCounter;
    }

    public void setAssignmentsCounter(Long assignmentsCounter) {
        this.assignmentsCounter = assignmentsCounter;
    }

    public List<Assignment> getAssignments() {
        return assignments;
    }

    public void setAssignments(List<Assignment> assignments) {
        this.assignments = assignments;
    }


    @Override
    public String toString() {
        return "AssignmentsResponse{" +
                "assignmentsCounter=" + assignmentsCounter +
                ", assignments=" + assignments +
                '}';
    }

}
