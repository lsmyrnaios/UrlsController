package eu.openaire.urls_controller.models;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "name"
})
public class Datasource {

    @JsonProperty("id")
    String id;

    @JsonProperty("name")
    String name;

    public Datasource() {}

    public Datasource(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
        return "Datasource{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
