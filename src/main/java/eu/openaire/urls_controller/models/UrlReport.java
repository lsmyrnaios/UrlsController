package eu.openaire.urls_controller.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "status",
        "payload",
        "error"
})
public class UrlReport {

    public enum StatusType {
        accessible, non_accessible
    }

    @JsonProperty("status")
    private StatusType status;

    @JsonProperty("payload")
    private Payload payload;

    @JsonProperty("error")
    private Error error;


    public UrlReport(StatusType status, Payload payload, Error error) {
        this.status = status;
        this.payload = payload;
        this.error = error;
    }


    public StatusType getStatus() {
        return this.status;
    }

    public void setStatus(StatusType status) {
        this.status = status;
    }

    public Payload getPayload() {
        return payload;
    }

    public void setPayload(Payload payload) {
        this.payload = payload;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "UrlReport{" +
                "status=" + status +
                ", payload=" + payload +
                ", error=" + error +
                '}';
    }
}
