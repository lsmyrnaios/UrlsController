package eu.openaire.urls_controller.models;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "message",
        "reportID"
})
public class BulkImportResponse {

    @JsonProperty("message")
    String message;

    @JsonProperty("reportID")
    String reportID;

    public BulkImportResponse() {}

    public BulkImportResponse(String message, String bulkImportReportID) {
        this.message = message;
        this.reportID = bulkImportReportID;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getReportID() {
        return reportID;
    }

    public void setReportID(String reportID) {
        this.reportID = reportID;
    }

    @Override
    public String toString() {
        return "BulkImportResponse{" +
                "message='" + message + '\'' +
                ", reportID='" + reportID + '\'' +
                '}';
    }
}
