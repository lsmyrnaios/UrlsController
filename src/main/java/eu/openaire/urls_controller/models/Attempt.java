package eu.openaire.urls_controller.models;

public class Attempt {

    private String id;

    private String original_url;

    private long dateMillis;

    private String status;

    private String error_class;

    private String error_message;

    public Attempt(String id, String original_url, long dateMillis, String status, String error_class, String error_message) {
        this.id = id;
        this.original_url = original_url;
        this.dateMillis = dateMillis;
        this.status = status;
        this.error_class = error_class;
        this.error_message = error_message;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOriginal_url() {
        return original_url;
    }

    public void setOriginal_url(String original_url) {
        this.original_url = original_url;
    }


    public long getDateMillis() {
        return dateMillis;
    }

    public void setDateMillis(long dateMillis) {
        this.dateMillis = dateMillis;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getError_class() {
        return error_class;
    }

    public void setError_class(String error_class) {
        this.error_class = error_class;
    }

    public String getError_message() {
        return error_message;
    }

    public void setError_message(String error_message) {
        this.error_message = error_message;
    }


    @Override
    public String toString() {
        return "Attempt{" +
                "id='" + id + '\'' +
                ", original_url='" + original_url + '\'' +
                ", dateMillis=" + dateMillis +
                ", status='" + status + '\'' +
                ", error_class='" + error_class + '\'' +
                ", error_message='" + error_message + '\'' +
                '}';
    }
}
