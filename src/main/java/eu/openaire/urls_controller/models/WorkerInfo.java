package eu.openaire.urls_controller.models;

public class WorkerInfo {


    String workerIP;

    boolean hasShutdown;

    // TODO - Add other info


    public WorkerInfo() {
    }


    public WorkerInfo(String workerIP, boolean hasShutdown) {
        this.workerIP = workerIP;
        this.hasShutdown = hasShutdown;
    }

    public String getWorkerIP() {
        return workerIP;
    }

    public void setWorkerIP(String workerIP) {
        this.workerIP = workerIP;
    }


    public boolean getHasShutdown() {
        return hasShutdown;
    }

    public void setHasShutdown(boolean hasShutdown) {
        this.hasShutdown = hasShutdown;
    }


    @Override
    public String toString() {
        return "WorkerInfo{" +
                "workerIP='" + workerIP + '\'' +
                ", hasShutdown=" + hasShutdown +
                '}';
    }
}
