package eu.openaire.urls_controller.models;

public class ParquetReport {

    public enum ParquetType {attempt, payload};

    private ParquetType parquetType;

    private boolean successful;


    public ParquetReport(ParquetType parquetType, boolean successful) {
        this.parquetType = parquetType;
        this.successful = successful;
    }

    public ParquetType getParquetType() {
        return parquetType;
    }

    public boolean isSuccessful() {
        return successful;
    }


    @Override
    public String toString() {
        return "ParquetReport{" +
                "parquetType=" + parquetType +
                ", wasSuccessful=" + successful +
                '}';
    }

}
