package eu.openaire.urls_controller.models;

import org.springframework.http.ResponseEntity;

public class SumParquetSuccess {

    private boolean attemptParquetFileProblem;

    private boolean payloadParquetFileProblem;

    private ResponseEntity<?> responseEntity;

    public SumParquetSuccess(boolean attemptParquetFileProblem, boolean payloadParquetFileProblem, ResponseEntity<?> responseEntity) {
        this.attemptParquetFileProblem = attemptParquetFileProblem;
        this.payloadParquetFileProblem = payloadParquetFileProblem;
        this.responseEntity = responseEntity;
    }

    public boolean isAttemptParquetFileProblem() {
        return attemptParquetFileProblem;
    }

    public boolean isPayloadParquetFileProblem() {
        return payloadParquetFileProblem;
    }

    public ResponseEntity<?> getResponseEntity() {
        return responseEntity;
    }


    @Override
    public String toString() {
        return "SumParquetSuccess{" +
                "attemptParquetFileProblem=" + attemptParquetFileProblem +
                ", payloadParquetFileProblem=" + payloadParquetFileProblem +
                ", responseEntity=" + responseEntity +
                '}';
    }

}
