package eu.openaire.urls_controller.models;

import eu.openaire.urls_controller.util.FileUtils;

import java.util.regex.Matcher;

public class FileLocationData {

    String fileDir;

    String fileName;

    String filenameWithoutExtension;

    String fileNameID;

    String dotFileExtension;

    public FileLocationData(String fileLocation) throws RuntimeException {
        // Extract and set LocationData.
        Matcher matcher = FileUtils.FILEPATH_ID_EXTENSION.matcher(fileLocation);
        if ( !matcher.matches() )
            throw new RuntimeException("Failed to match the \"" + fileLocation + "\" with the regex: " + FileUtils.FILEPATH_ID_EXTENSION);
        fileDir = matcher.group(1);
        if ( (fileDir == null) || fileDir.isEmpty() )
            throw new RuntimeException("Failed to extract the \"fileDir\" from \"" + fileLocation + "\".");
        fileName = matcher.group(2);
        if ( (fileName == null) || fileName.isEmpty() )
            throw new RuntimeException("Failed to extract the \"fileName\" from \"" + fileLocation + "\".");
        // The "matcher.group(3)" returns the "filenameWithoutExtension", which is currently not used.
        fileNameID = matcher.group(4);
        if ( (fileNameID == null) || fileNameID.isEmpty() )
            throw new RuntimeException("Failed to extract the \"fileNameID\" from \"" + fileLocation + "\".");
        dotFileExtension = matcher.group(5);
        if ( (dotFileExtension == null) || dotFileExtension.isEmpty() )
            throw new RuntimeException("Failed to extract the \"dotFileExtension\" from \"" + fileLocation + "\".");
    }

    public String getFileDir() {
        return fileDir;
    }

    public void setFileDir(String fileDir) {
        this.fileDir = fileDir;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFilenameWithoutExtension() {
        return filenameWithoutExtension;
    }

    public void setFilenameWithoutExtension(String filenameWithoutExtension) {
        this.filenameWithoutExtension = filenameWithoutExtension;
    }

    public String getFileNameID() {
        return fileNameID;
    }

    public void setFileNameID(String fileNameID) {
        this.fileNameID = fileNameID;
    }

    public String getDotFileExtension() {
        return dotFileExtension;
    }

    public void setDotFileExtension(String dotFileExtension) {
        this.dotFileExtension = dotFileExtension;
    }

    @Override
    public String toString() {
        return "FileLocationData{" +
                "fileDir='" + fileDir + '\'' +
                ", fileName='" + fileName + '\'' +
                ", filenameWithoutExtension='" + filenameWithoutExtension + '\'' +
                ", fileNameID='" + fileNameID + '\'' +
                ", dotFileExtension='" + dotFileExtension + '\'' +
                '}';
    }
}
