package eu.openaire.urls_controller.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "url",
        "datasource"
})
public class Task {

    @JsonProperty("id")
    private String id;

    @JsonProperty("url")
    private String url;

    @JsonProperty("datasource")
    private Datasource datasource;

    public Task() {}

    public Task(String id, String url, Datasource datasource) {
        this.id = id;
        this.url = url;
        this.datasource = datasource;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Datasource getDatasource() {
        return datasource;
    }

    public void setDatasource(Datasource datasource) {
        this.datasource = datasource;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id='" + id + '\'' +
                ", url='" + url + '\'' +
                ", datasource=" + datasource +
                '}';
    }
}
