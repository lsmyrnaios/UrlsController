package eu.openaire.urls_controller.components;


import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@ConfigurationProperties(prefix = "bulk-import")
public class BulkImport {

    private String baseBulkImportLocation;

    private String bulkImportReportLocation;

    private int numOfThreadsForBulkImportProcedures;

    private Map<String, BulkImportSource> bulkImportSources;

    public BulkImport() {
    }

    public String getBaseBulkImportLocation() {
        return baseBulkImportLocation;
    }

    public void setBaseBulkImportLocation(String baseBulkImportLocation) {
        this.baseBulkImportLocation = baseBulkImportLocation;
    }

    public String getBulkImportReportLocation() {
        return bulkImportReportLocation;
    }

    public void setBulkImportReportLocation(String bulkImportReportLocation) {
        this.bulkImportReportLocation = bulkImportReportLocation;
    }

    public int getNumOfThreadsForBulkImportProcedures() {
        return numOfThreadsForBulkImportProcedures;
    }

    public void setNumOfThreadsForBulkImportProcedures(int numOfThreadsForBulkImportProcedures) {
        this.numOfThreadsForBulkImportProcedures = numOfThreadsForBulkImportProcedures;
    }

    public Map<String, BulkImportSource> getBulkImportSources() {
        return bulkImportSources;
    }

    public void setBulkImportSources(Map<String, BulkImportSource> bulkImportSources) {
        this.bulkImportSources = bulkImportSources;
    }

    @Override
    public String toString() {
        return "BulkImport{" +
                "baseBulkImportLocation='" + baseBulkImportLocation + '\'' +
                ", bulkImportReportLocation='" + bulkImportReportLocation + '\'' +
                ", numOfThreadsForBulkImportProcedures=" + numOfThreadsForBulkImportProcedures +
                ", bulkImportSources=" + bulkImportSources +
                '}';
    }


    public static class BulkImportSource {
        private String datasourceID;
        private String datasourcePrefix;
        private String fulltextUrlPrefix;
        private String mimeType;
        private String idMappingFilePath;
        private boolean isAuthoritative;


        public BulkImportSource() {
        }

        public String getDatasourceID() {
            return datasourceID;
        }

        public void setDatasourceID(String datasourceID) {
            this.datasourceID = (datasourceID.isEmpty() ? null : datasourceID);
        }

        public String getDatasourcePrefix() {
            return datasourcePrefix;
        }

        public void setDatasourcePrefix(String datasourcePrefix) {
            this.datasourcePrefix = (datasourcePrefix.isEmpty() ? null : datasourcePrefix);
        }

        public String getFulltextUrlPrefix() {
            return fulltextUrlPrefix;
        }

        public void setFulltextUrlPrefix(String fulltextUrlPrefix) {
            this.fulltextUrlPrefix = (fulltextUrlPrefix.isEmpty() ? null : fulltextUrlPrefix);
        }

        public String getMimeType() {
            return mimeType;
        }

        public void setMimeType(String mimeType) {
            this.mimeType = (mimeType.isEmpty() ? null : mimeType);
        }

        public String getIdMappingFilePath() {
            return idMappingFilePath;
        }

        public void setIdMappingFilePath(String idMappingFilePath) {
            this.idMappingFilePath = (idMappingFilePath.isEmpty() ? null : idMappingFilePath);
        }

        public boolean getIsAuthoritative() {
            return isAuthoritative;
        }

        public void setIsAuthoritative(boolean isAuthoritative) {
            this.isAuthoritative = isAuthoritative;
        }

        @Override
        public String toString() {
            return "BulkImportSource{" + "datasourceID='" + datasourceID + '\'' + ", datasourcePrefix='" + datasourcePrefix + '\'' + ", fulltextUrlPrefix='" + fulltextUrlPrefix + '\'' + ", mimeType='" + mimeType + '\'' +
                    ", idMappingFilePath='" + idMappingFilePath + '\'' +
                    ", isAuthoritative=" + isAuthoritative + '}';
        }
    }

}
