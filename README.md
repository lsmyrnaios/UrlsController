# UrlsController
## [![Jenkins build status](https://jenkins-dnet.d4science.org/buildStatus/icon?job=UrlsController)](https://jenkins-dnet.d4science.org/job/UrlsController/)

The Controller's Application receives requests coming from the [**Workers**](https://code-repo.d4science.org/lsmyrnaios/UrlsWorker) (deployed on the cloud), constructs an assignments-list with data received from a database and returns the list to the workers.<br>
Then, it receives the "WorkerReports", it requests the full-texts from the workers, in batches, and uploads them on the S3-Object-Store. Finally, it writes the related reports, along with the updated file-locations into the database.<br> 
<br>
It can also process **Bulk-Import** requests, from compatible data sources, in which case it receives the full-text files immediately, without offloading crawling jobs to Workers.<br>
<br>
For managing and generating data, we use [**Impala**](https://impala.apache.org/) JDBC and WebHDFS.<br>
<br>


### To install and run the application:
- Run ```git clone``` and then ```cd UrlsController```.
- Set the preferable values inside the [__application.yml__](https://code-repo.d4science.org/lsmyrnaios/UrlsController/src/branch/master/src/main/resources/application.yml) file. Specifically, for tests, set the ***services.pdfaggregation.controller.isTestEnvironment*** property to "**true**" and make sure the "***services.pdfaggregation.controller.db.testDatabaseName***" property is set to a test-database.
- Execute the ```installAndRun.sh``` script which builds and runs the app.<br>
  If you want to just run the app, then run the script with the argument "1": ```./installAndRun.sh 1```.<br>
  If you want to build and run the app on a **Docker Container**, then run the script with the argument "0" followed by the argument "1": ```./installAndRun.sh 0 1```.<br>
  Additionally, if you want to test/visualize the exposed metrics on Prometheus and Grafana, you can deploy their instances on docker containers,
  by enabling the "runPrometheusAndGrafanaContainers" switch, inside the "./installAndRun.sh" script.<br>
  <br>


### BulkImport API:
- "**bulkImportFullTexts**" endpoint: **http://\<IP\>:\<PORT\>/api/bulkImportFullTexts?provenance=\<provenance\>&bulkImportDir=\<bulkImportDir\>&shouldDeleteFilesOnFinish={true|false}** <br>
  This endpoint loads the right configuration with the help of the "provenance" parameter, delegates the processing to a background thread and immediately returns a message with useful information, including the "reportFileID", which can be used at any moment to request a report about the progress of the bulk-import procedure. Use the **_HTTP POST_** method to access the endpoint.<br>
  The processing job starts running after 30-60 minutes and processes the full-texts files inside the given directory, in the following way: it generates the OpenAIRE-IDs, uploads the files to the S3 Object Store, generates and stores the "payload" records in the database. If it is requested by the user, it removes the successfully imported full-texts from the directory.
- "**getBulkImportReport**" endpoint: **http://\<IP\>:\<PORT\>/api/getBulkImportReport?id=\<reportFileID\>** <br>
  This endpoint returns the bulkImport report, which corresponds to the given reportFileID, in JSON format.
<br>

#### How to add a bulk-import datasource:
- Open the [__application.yml__](https://code-repo.d4science.org/lsmyrnaios/UrlsController/src/branch/master/src/main/resources/application.yml) file.
- Add a new object under the "bulk-import.bulkImportSources" property.
- Read the comments written in the end of the "bulk-import" property and make sure all requirements are met. 
<br>
<br>

### Statistics API:
- "**getNumberOfAllPayloads**" endpoint: **http://\<IP\>:\<PORT\>/api/stats/getNumberOfAllPayloads** <br>
  This endpoint returns the total number of payloads existing in the database, independently of the way they were aggregated. This includes the payloads created by other pieces of software, before the PDF-Aggregation-Service was created.
- "**getNumberOfPayloadsAggregatedByServiceThroughCrawling**" endpoint: **http://\<IP\>:\<PORT\>/api/stats/getNumberOfPayloadsAggregatedByServiceThroughCrawling** <br>
  This endpoint returns the number of payloads aggregated by the PDF-Aggregated-Service itself, through crawling.
- "**getNumberOfPayloadsAggregatedByServiceThroughBulkImport**" endpoint: **http://\<IP\>:\<PORT\>/api/stats/getNumberOfPayloadsAggregatedByServiceThroughBulkImport?provenance=<bulk_import-provenance>** <br>
  This endpoint returns the number of payloads aggregated by the PDF-Aggregated-Service itself, through bulk-import procedures, from compatible datasources.
  The "provenance" request-parameter is optional and can be provided to specify the bulk_import-provenance for which we want to take the number of payloads. (the value of the parameter should not include the "bulk:" prefix)
- "**getNumberOfPayloadsAggregatedByService**" endpoint: **http://\<IP\>:\<PORT\>/api/stats/getNumberOfPayloadsAggregatedByService** <br>
  This endpoint returns the number of payloads aggregated by the PDF-Aggregated-Service itself, both through crawling and bulk-import procedures.
- "**getNumberOfLegacyPayloads**" endpoint: **http://\<IP\>:\<PORT\>/api/stats/getNumberOfLegacyPayloads** <br>
  This endpoint returns the number of payloads which were aggregated by methods other than the PDF Aggregation Service.
- "**getNumberOfPayloadsForDatasource**" endpoint:  **http://\<IP\>:\<PORT\>/api/stats/getNumberOfPayloadsForDatasource?datasourceId=\<givenDatasourceId\>** <br>
  This endpoint returns the number of payloads which belong to the datasource specified by the given datasourceID.
- "**getNumberOfRecordsInspectedByServiceThroughCrawling**" endpoint: **http://\<IP\>:\<PORT\>/api/stats/getNumberOfRecordsInspectedByServiceThroughCrawling** <br>
  This endpoint returns the number of records inspected by the PDF-Aggregation-Service through crawling.
<br>
<br>

### Shutdown Service API:
- "**shutdownService**" endpoint: **http://localhost:\<PORT\>/api/shutdownService** <br>
  This endpoint sends "shutdownWorker" requests to all the Workers which are actively participating in the Service. The Workers will shut down after finishing their work-in-progress and all full-texts have been either transferred to the Controller or deleted, in case an error has appeared.<br>
  Once the Workers are about to shut down, they send a "shutdownReport" to the Controller. A scheduling task runs in the Controller, every 2 hours, and if the user has specified that the Controller must shut down and all the Workers participating in the Service have shutdown, then it gently shuts down the Controller. 
- "**cancelShutdownService**" endpoint: **http://localhost:\<PORT\>/api/cancelShutdownService** <br>
  This endpoint specifies that the Controller will not shut down, and sends "cancelShutdownWorker" requests to all the Workers which are actively participating in the Service (have not shut down yet), so that they can continue to request assignments.
- "**shutdownAllWorkers**" endpoint: **http://localhost:\<PORT\>/api/shutdownAllWorkers** <br>
  This endpoint sends "shutdownWorker" requests to all the Workers which are actively participating in the Service. The Workers will shut down after finishing their work-in-progress and all full-texts have been either transferred to the Controller or deleted, in case an error has appeared.<br>
  Once the Workers are about to shut down, they send a "shutdownReport" to the Controller. <br>
  This endpoint is helpful when we want to update only the Workers, while keeping the Service running for Bulk-import procedures.
- "**cancelShutdownAllWorkers**" endpoint: **http://localhost:\<PORT\>/api/cancelShutdownAllWorkers** <br>
  This endpoint specifies that the Workers will not shut down, and sends "cancelShutdownWorker" requests to all the Workers which are actively participating in the Service (have not shut down yet), so that they can continue to request assignments.
<br>

#### Notes:
- The Shutdown Service API is accessible by the Controller's host machine.<br>
- Use the **_HTTP POST_** method to access the endpoints.
<br>
<br>


### Prometheus Metrics:
- "**num_all_payloads**"
- "**num_payloads_aggregated_by_service_through_crawling**"
- "**num_payloads_aggregated_by_service_through_bulk_import**"
- "**num_payloads_aggregated_by_service_through_bulk_import_arxiv**"
- "**num_payloads_aggregated_by_service_through_bulk_import_springer**"
- "**num_payloads_aggregated_by_service_through_bulk_import_europepmc**"
- "**num_payloads_aggregated_by_service_through_bulk_import_wileyml**"****
- "**num_payloads_aggregated_by_service**"
- "**num_legacy_payloads**"
- "**num_records_inspected_by_service_through_crawling**"
- "**average_fulltexts_transfer_size_of_worker_reports**"
- "**average_success_percentage_of_worker_reports**"
- "**getAssignments_time_seconds_max**": Time taken to return the assignments.
- "**addWorkerReport_time_seconds**": Time taken to add the WorkerReport.
<br>
<br>


#### Implementation notes:
- For transferring the full-text files, we use Facebook's [**Zstandard**](https://facebook.github.io/zstd/) compression algorithm, which brings very big benefits in compression rate and speed.
- The uploaded full-text files follow this naming-scheme: "**datasourceID/recordID::fileHash.pdf**"
